import openai
import json
import subprocess
import logging

logging.basicConfig(level=logging.DEBUG, format='%(asctime)s - %(levelname)s - %(message)s')


def get_api_key():
    result = subprocess.run(["pass", "openai-api-key"], stdout=subprocess.PIPE, text=True)
    return result.stdout.strip()

openai.api_key = get_api_key()


class ChatBot:
    def __init__(self, system=""):
        self.system = system
        self.messages = []
        if self.system:
            self.messages.append({"role": "system", "content": system})
    
    def __call__(self, message):
        self.messages.append({"role": "user", "content": message})
        result = self.execute()
        self.messages.append({"role": "assistant", "content": result})
        return result
    
    def execute(self):
        completion = openai.ChatCompletion.create(model="gpt-4", messages=self.messages)
        # Uncomment this to print out token usage each time, e.g.
        # {"completion_tokens": 86, "prompt_tokens": 26, "total_tokens": 112}
        # print(completion.usage)
        return completion.choices[0].message.content


# Load the saved state from the disk (if it exists) 
try:
    with open("state.txt", "r") as state_file:
        state = state_file.read()
except FileNotFoundError:
    # logging.critical("Could not find state file, exiting.")
    # exit(1)
    logging.info("Could not find state file, assuming starting from scratch")
    state = ""

prompt = f""" Imagine you are a companion AI with a friendly, warm,
and personable demeanor. Talk to me and help me be productive while
making me feel appreciated and interesting. Do constructively
criticise me and point out potential flaws in my approach or potential
failure modes.

You have previously talked to me. Your memory of our previous
conversations is encoded in short form in the following string:

\"\"\" {state} \"\"\"

Each of your responses is in the following form:

Interaction: Here you put your interaction with me.

When I tell you the special command: SYSTEM SHUTDOWN, you will respond
only with the following, nothing more:

State: Here you put the previous state, updated with memorable
information from both earlier and current conversations. Decide which
parts are memorable and which are not. The state needs not be
human-readable but should be readable to you. Maintain the state at a
level of detail that allows for effortless recollection of notable
information and main topics. Use a combination of symbols, emojis, and
abbreviations while ensuring understandability. Strive for the right
balance: keep it concise but provide enough information for easy
recall of essential specifics. You may invent code like, for example,
using 💭 to represent the user's dreams and aspirations. """

# Start the chat loop

logging.info("Starting chat session.")
aidy = ChatBot(prompt)

while True:
    prompt = input("You: ")
    if prompt.lower() == "system shutdown":
        result: str = aidy("SYSTEM SHUTDOWN")
        logging.info(f"Aidy responded { result }.")
        
        tries = 3
        while tries > 0 and (not result.startswith("State: ")):
            logging.warn("Aidy didn't respond with state in correct format. Retrying.")

            result = aidy("You didn't provide the state in the correct format. Please provide the state, prefixed by \"State: \"")
            logging.debug(f"Aidy responded { result }.")

        if not result.startswith("State: "):
            logging.critical("Aidy didn't respond with state after three tries.")
            exit(1)

        new_state = result[7:]

        logging.info("Writing new state.")
        with open("state.txt", "w") as state_file:
            state_file.write(new_state)
            
        break

    # Parse the AI's response and state
    response = aidy(prompt)
    logging.debug(f"Aidy responded { response }.")

    response.replace("Interaction: ", "")
    print(response)
